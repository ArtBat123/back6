<html>
<head>
    <script src="https://kit.fontawesome.com/e2ac9cc532.js" crossorigin="anonymous"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Форма с базой данных</title>
    <link  href="style-form.css" rel="stylesheet"  media="all"/>
</head>
<div class="form-container">
<body>
<?php
if (!empty($messages)) {
    print('<div id="messages">');
    // Выводим все сообщения.
    foreach ($messages as $message) {
        print($message);
    }
    print('</div>');
}

// Далее выводим форму отмечая элементы с ошибками классом error
// и задавая начальные значения элементов ранее сохраненными.
?>
<div class="in-form-container">
<form action="index.php" accept-charset="UTF-8" method="POST">


    <div class="set">

        <div class="formname">
            <label>
                
                Ваше имя
    <input   class="formname" type="text"  name="fio" placeholder="Введите имя"
         value="<?php print $values['fio']; ?>">
            </label>

        </div>

        <div class="form_mail">

        <label>
           
            Ваша почта</label>
    <input class="formmail" type="email" name="email" placeholder="Введите почту"  value="<?php print $values['email']; ?>">
        </div>

</div>


    <div class="set">

        <div class="birthday">
    <label >
        

        Год рождения<br/>
        <input placeholder="Введите год рождения"  value="<?php print $values['year_value']; ?>" id="dr" name="birthyear"
        type="number"/>
    </label>
    </div>

        <div>
        <div class="limbs" <?php if($errors['limb']){print 'style="margin-left:53px;margin-top:4px"';}?>>
           
        Кол-во конечностей<br/>
            <div class="radio-container">
        <input <?php if($values['limb_value']=="3"){print 'checked';}?> type="radio" id="3l" name="radio1" value="3"/>
            <label for="3l">3</label>
        <input <?php if($values['limb_value']=="4"){print 'checked';}?> type="radio" id="4l" name="radio1" value="4"/>
            <label for="4l">4</label>
        </div>
        </div>
    </div>
    </div>


    <div class="set_vertical">

        <div class="gender">
            
    Выберите пол:

            <div class="radio-container">
            <input <?php if($values['sex_value']=="man"){print 'checked';}?> type="radio" id="male" name="radio2" value="man" />
                <label for="male">Мужской </label>

            <input <?php if($values['sex_value']=="woman"){print 'checked';}?> type="radio" id="female" name="radio2" value="woman"/>
                <label for="female">Женский</label>
            </div>


     <div class="abil">
      <label >
         
        Cверхспособности
        <br/>
         <div >
            <select id="sp" name="select1[]" multiple="multiple">

                <option  <?php if($values['abil_value']=="god"){print 'selected';}?>value="god">Бессмертие</option>
                <option  <?php if($values['abil_value']=="wall"){print 'selected';}?>value="wall">Прохожение сквозь стены</option>
                <option  <?php if($values['abil_value']=="levity"){print 'selected';}?>value="levity">Левитация</option>
                <option  <?php if($values['abil_value']=="kos"){print 'selected';}?>value="kos">Понимать дискретку</option>
                <option  <?php if($values['abil_value']=="kol"){print 'selected';}?>value="kol">Понимать диффуры</option>

            </select>
         </div>
      </label>
     </div>
 </div>
    <div class="biogr">
        <label> 
            Напишите про себя
        </label>
        <textarea id="biog" name="textarea1" placeholder="Пиши тут"><?php print $values['bio_value'];?></textarea>
    </div>
</div>

    <div class="set">

        <div>
            <div class="set_check">
                <div class="check_text">
            <label >
               
                Согласие на обработку</br>
                персональных данных</label>
                </div>
            </div>
        </div>

            <input 
                <?php if($values['check_value']=="1"){print 'checked';}?>id="formcheck" type="checkbox" name="checkbox" value="1">


    <input   type="submit" id="send" class="buttonform" value="Отправить">
    </div>

</div>
</form>
</div>
</body>
</div>
</html>
